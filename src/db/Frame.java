package db;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.bson.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoNamespace;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Indexes;

import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Updates.*;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextPane;
import javax.swing.JTextField;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.List;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JToggleButton;
import javax.swing.ListSelectionModel;
import javax.swing.JSeparator;
import javax.swing.JList;

public class Frame extends JFrame {

	// Código de serialización
	private static final long serialVersionUID = -5885125365384882677L;

	MongoClient mongoClient = new MongoClient("localhost", 27017);

	private JPanel contentPane;
	private JTextField tfQuerry;

	private MongoDatabase database = null;
	private MongoCollection<Document> collection = null;

	private HashMap<String, String> informacion = new HashMap<String, String>();
	private HashMap<String, String> tipoQuerry = new HashMap<String, String>();

	@SuppressWarnings("deprecation")
	private List<String> listDB = mongoClient.getDatabaseNames();

	private JTextField tfUpdate;
	private JTextField tfUpdateNuevoValor;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Frame frame = new Frame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}
	
	
	//Método para saber si el valor introducido es numerico (int)
	public boolean isNumeric(String s) {
		return s != null && s.matches("[-+]?\\d*\\.?\\d+");
	}

	

	public boolean documentoContenido(MongoDatabase database, MongoCollection<Document> collection, String clave,
			String valor, String comparador) {

		boolean result = false;

		final String contenido = querry(database, collection, clave, valor, comparador);

		if (contenido.equals("Lo siento, pero no se ha podido encontrar ningún resultado con la consulta solicitada")) {
			result = false;
		} else {
			result = true;
		}

		return result;
	}

	// Método para otener el número de veces que se repite un documento que cumple
	// una cierta condición
	public int numeroElementosIguales(MongoDatabase database, MongoCollection<Document> collection, String clave,
			String valor) {

		int contador = 0;

		if (isNumeric(valor)) {
			int valorInt = Integer.parseInt(valor);
			final FindIterable<Document> fi = collection.find(Filters.eq(clave, valorInt));
			final MongoCursor<Document> cursor = fi.iterator();

			try {
				while (cursor.hasNext()) {
					contador++;
				}
			} finally {
				cursor.close();
			}
		} else {
			final FindIterable<Document> fi = collection.find(Filters.eq(clave, valor));
			final MongoCursor<Document> cursor = fi.iterator();

			try {
				while (cursor.hasNext()) {
					contador++;
				}
			} finally {
				cursor.close();
			}
		}

		return contador;
	}

	// Método para ejecutar la querry (eq) sobre los documentos de una colección
	public String querry(MongoDatabase database, MongoCollection<Document> collection, String clave, String valor,
			String comparador) {

		String result = "Lo siento, pero no se ha podido encontrar ningún resultado con la consulta solicitada";

		final StringBuffer buffer = new StringBuffer();

		FindIterable<Document> fi = null;

		// "=/eq",">/gt","≥/gte","</lt","≤/lte","!=/ne","in","nin"

		if (isNumeric(valor)) {
			int valorInt = Integer.parseInt(valor);

			if (comparador.equals("= / eq")) {
				fi = collection.find(Filters.eq(clave, valorInt));
			} else if (comparador.equals("> / gt")) {
				fi = collection.find(Filters.gt(clave, valorInt));
			} else if (comparador.equals("≥ / gte")) {
				fi = collection.find(Filters.gte(clave, valorInt));
			} else if (comparador.equals("< / lt")) {
				fi = collection.find(Filters.lt(clave, valorInt));
			} else if (comparador.equals("≤ / lte")) {
				fi = collection.find(Filters.lte(clave, valorInt));
			} else if (comparador.equals("!= / ne")) {
				fi = collection.find(Filters.ne(clave, valorInt));
			} else if (comparador.equals("!() in")) {
				fi = collection.find(Filters.in(clave, valorInt));
			} else if (comparador.equals("() nin")) {
				fi = collection.find(Filters.nin(clave, valorInt));
			}

		} else {
			if (comparador.equals("= / eq")) {
				fi = collection.find(Filters.eq(clave, valor));
			} else if (comparador.equals("> / gt")) {
				fi = collection.find(Filters.gt(clave, valor));
			} else if (comparador.equals("≥ / gte")) {
				fi = collection.find(Filters.gte(clave, valor));
			} else if (comparador.equals("< / lt")) {
				fi = collection.find(Filters.lt(clave, valor));
			} else if (comparador.equals("≤ / lte")) {
				fi = collection.find(Filters.lte(clave, valor));
			} else if (comparador.equals("!= / ne")) {
				fi = collection.find(Filters.ne(clave, valor));
			} else if (comparador.equals("() in")) {
				fi = collection.find(Filters.in(clave, valor));
			} else if (comparador.equals("!() nin")) {
				fi = collection.find(Filters.nin(clave, valor));
			}
		}

		if (fi != null) {
			MongoCursor<Document> cursor = fi.iterator();
			try {
				while (cursor.hasNext()) {
					buffer.append(cursor.next().toJson() + "\n");
				}
			} finally {
				cursor.close();
			}
		} else {
			result = "Lo siento, pero debes seleccionar un comparador para ejecutar la querry";
		}

		if (buffer.toString().equals("")) {
			result = "Lo siento, pero no se ha podido encontrar ningún resultado con la consulta solicitada";
		} else {
			result = buffer.toString();
		}

		return result;
	}

	// Método para ejecutar la querry sobre todos los documentos de una colección
	public String querryTodos(MongoDatabase database, MongoCollection<Document> collection) {

		String result = "Lo siento, pero no se ha podido encontrar ningún resultado con la consulta solicitada";

		StringBuffer buffer = new StringBuffer();

		FindIterable<Document> fi = collection.find();
		MongoCursor<Document> cursor = fi.iterator();
		try {
			while (cursor.hasNext()) {
				buffer.append(cursor.next().toJson() + "\n");
			}
		} finally {
			cursor.close();
		}

		if (buffer.toString().equals("")) {
			result = "Lo siento, pero no se ha podido encontrar ningún resultado con la consulta solicitada";
		} else {
			result = buffer.toString();
		}

		return result;
	}

	// Método para actualizar documentos de una colección
	public boolean actualizarDocumento(MongoDatabase database, MongoCollection<Document> collection, String clave,
			String valor, String nuevoValor, String comparador, boolean filtro) {

		// int vecesRepiteDocumeno = numeroElementosIguales(database, collection, clave,
		// valor);

		boolean result = false;

		final boolean contenido = documentoContenido(database, collection, clave, valor, comparador);

		if (contenido) {

			if (filtro) {
				if (isNumeric(valor)) {

					int valorInt = Integer.parseInt(valor);

					if (tipoQuerry.get("Actualizar").equals("Uno")) {

						if (comparador.equals("= / eq")) {
							collection.updateOne(eq(clave, valorInt), set(clave, Integer.parseInt(nuevoValor)));
						} else if (comparador.equals("> / gt")) {
							collection.updateOne(gt(clave, valorInt), set(clave, Integer.parseInt(nuevoValor)));
						} else if (comparador.equals("≥ / gte")) {
							collection.updateOne(gte(clave, valorInt), set(clave, Integer.parseInt(nuevoValor)));
						} else if (comparador.equals("< / lt")) {
							collection.updateOne(lt(clave, valorInt), set(clave, Integer.parseInt(nuevoValor)));
						} else if (comparador.equals("≤ / lte")) {
							collection.updateOne(lte(clave, valorInt), set(clave, Integer.parseInt(nuevoValor)));
						} else if (comparador.equals("!= / ne")) {
							collection.updateOne(ne(clave, valorInt), set(clave, Integer.parseInt(nuevoValor)));
						} else if (comparador.equals("() in")) {
							collection.updateOne(in(clave, valorInt), set(clave, Integer.parseInt(nuevoValor)));
						} else if (comparador.equals("!() nin")) {
							collection.updateOne(nin(clave, valorInt), set(clave, Integer.parseInt(nuevoValor)));
						}
					} else {

						if (comparador.equals("= / eq")) {
							collection.updateMany(eq(clave, valorInt), set(clave, Integer.parseInt(nuevoValor)));
						} else if (comparador.equals("> / gt")) {
							collection.updateMany(gt(clave, valorInt), set(clave, Integer.parseInt(nuevoValor)));
						} else if (comparador.equals("≥ / gte")) {
							collection.updateMany(gte(clave, valorInt), set(clave, Integer.parseInt(nuevoValor)));
						} else if (comparador.equals("< / lt")) {
							collection.updateMany(lt(clave, valorInt), set(clave, Integer.parseInt(nuevoValor)));
						} else if (comparador.equals("≤ / lte")) {
							collection.updateMany(lte(clave, valorInt), set(clave, Integer.parseInt(nuevoValor)));
						} else if (comparador.equals("!= / ne")) {
							collection.updateMany(ne(clave, valorInt), set(clave, Integer.parseInt(nuevoValor)));
						} else if (comparador.equals("() in")) {
							collection.updateMany(in(clave, valorInt), set(clave, Integer.parseInt(nuevoValor)));
						} else if (comparador.equals("!() nin")) {
							collection.updateMany(nin(clave, valorInt), set(clave, Integer.parseInt(nuevoValor)));
						}
					}
				} else {

					if (tipoQuerry.get("Actualizar").equals("Uno")) {

						if (comparador.equals("= / eq")) {
							collection.updateOne(eq(clave, valor), set(clave, Integer.parseInt(nuevoValor)));
						} else if (comparador.equals("> / gt")) {
							collection.updateOne(gt(clave, valor), set(clave, Integer.parseInt(nuevoValor)));
						} else if (comparador.equals("≥ / gte")) {
							collection.updateOne(gte(clave, valor), set(clave, Integer.parseInt(nuevoValor)));
						} else if (comparador.equals("< / lt")) {
							collection.updateOne(lt(clave, valor), set(clave, Integer.parseInt(nuevoValor)));
						} else if (comparador.equals("≤ / lte")) {
							collection.updateOne(lte(clave, valor), set(clave, Integer.parseInt(nuevoValor)));
						} else if (comparador.equals("!= / ne")) {
							collection.updateOne(ne(clave, valor), set(clave, Integer.parseInt(nuevoValor)));
						} else if (comparador.equals("() in")) {
							collection.updateOne(in(clave, valor), set(clave, Integer.parseInt(nuevoValor)));
						} else if (comparador.equals("!() nin")) {
							collection.updateOne(nin(clave, valor), set(clave, Integer.parseInt(nuevoValor)));
						}
					} else {

						if (comparador.equals("= / eq")) {
							collection.updateMany(eq(clave, valor), set(clave, nuevoValor));
						} else if (comparador.equals("> / gt")) {
							collection.updateMany(gt(clave, valor), set(clave, nuevoValor));
						} else if (comparador.equals("≥ / gte")) {
							collection.updateMany(gte(clave, valor), set(clave, nuevoValor));
						} else if (comparador.equals("< / lt")) {
							collection.updateMany(lt(clave, valor), set(clave, nuevoValor));
						} else if (comparador.equals("≤ / lte")) {
							collection.updateMany(lte(clave, valor), set(clave, nuevoValor));
						} else if (comparador.equals("!= / ne")) {
							collection.updateMany(ne(clave, valor), set(clave, nuevoValor));
						} else if (comparador.equals("() in")) {
							collection.updateMany(in(clave, valor), set(clave, nuevoValor));
						} else if (comparador.equals("!() nin")) {
							collection.updateMany(nin(clave, valor), set(clave, nuevoValor));
						}
					}
				}
			} else {
				if (isNumeric(valor)) {

					int valorInt = Integer.parseInt(valor);

					if (tipoQuerry.get("Actualizar").equals("Uno")) {
						collection.updateOne(eq(clave, valorInt), set(clave, Integer.parseInt(nuevoValor)));
					} else {
						collection.updateMany(eq(clave, valorInt), set(clave, Integer.parseInt(nuevoValor)));
					}
				} else {

					if (tipoQuerry.get("Actualizar").equals("Uno")) {
						collection.updateOne(eq(clave, valor), set(clave, nuevoValor));
					} else {
						collection.updateMany(eq(clave, valor), set(clave, nuevoValor));

					}
				}
			}

			result = true;
		}
		return result;
	}

	// Método para eliminar documentos de una colección
	public boolean eliminarDocumento(MongoDatabase database, MongoCollection<Document> collection, String clave,
			String valor, String comparador, boolean filtro) {

		boolean result = false;

		final boolean contenido = documentoContenido(database, collection, clave, valor, comparador);

		if (contenido) {

			if (filtro) {
				if (isNumeric(valor)) {

					int valorInt = Integer.parseInt(valor);

					if (tipoQuerry.get("Eliminar").equals("Uno")) {

						if (comparador.equals("= / eq")) {
							collection.deleteOne(eq(clave, valorInt));
						} else if (comparador.equals("> / gt")) {
							collection.deleteOne(gt(clave, valorInt));
						} else if (comparador.equals("≥ / gte")) {
							collection.deleteOne(gte(clave, valorInt));
						} else if (comparador.equals("< / lt")) {
							collection.deleteOne(lt(clave, valorInt));
						} else if (comparador.equals("≤ / lte")) {
							collection.deleteOne(lte(clave, valorInt));
						} else if (comparador.equals("!= / ne")) {
							collection.deleteOne(ne(clave, valorInt));
						} else if (comparador.equals("() in")) {
							collection.deleteOne(in(clave, valorInt));
						} else if (comparador.equals("!() nin")) {
							collection.deleteOne(nin(clave, valorInt));
						}
					} else {

						if (comparador.equals("= / eq")) {
							collection.deleteMany(eq(clave, valorInt));
						} else if (comparador.equals("> / gt")) {
							collection.deleteMany(gt(clave, valorInt));
						} else if (comparador.equals("≥ / gte")) {
							collection.deleteMany(gte(clave, valorInt));
						} else if (comparador.equals("< / lt")) {
							collection.deleteMany(lt(clave, valorInt));
						} else if (comparador.equals("≤ / lte")) {
							collection.deleteMany(lte(clave, valorInt));
						} else if (comparador.equals("!= / ne")) {
							collection.deleteMany(ne(clave, valorInt));
						} else if (comparador.equals("() in")) {
							collection.deleteMany(in(clave, valorInt));
						} else if (comparador.equals("!() nin")) {
							collection.deleteMany(nin(clave, valorInt));
						}
					}
				} else {

					if (tipoQuerry.get("Eliminar").equals("Uno")) {

						if (comparador.equals("= / eq")) {
							collection.deleteOne(eq(clave, valor));
						} else if (comparador.equals("> / gt")) {
							collection.deleteOne(gt(clave, valor));
						} else if (comparador.equals("≥ / gte")) {
							collection.deleteOne(gte(clave, valor));
						} else if (comparador.equals("< / lt")) {
							collection.deleteOne(lt(clave, valor));
						} else if (comparador.equals("≤ / lte")) {
							collection.deleteOne(lte(clave, valor));
						} else if (comparador.equals("!= / ne")) {
							collection.deleteOne(ne(clave, valor));
						} else if (comparador.equals("() in")) {
							collection.deleteOne(in(clave, valor));
						} else if (comparador.equals("!() nin")) {
							collection.deleteOne(nin(clave, valor));
						}
					} else {
						if (comparador.equals("= / eq")) {
							collection.deleteMany(eq(clave, valor));
						} else if (comparador.equals("> / gt")) {
							collection.deleteMany(gt(clave, valor));
						} else if (comparador.equals("≥ / gte")) {
							collection.deleteMany(gte(clave, valor));
						} else if (comparador.equals("< / lt")) {
							collection.deleteMany(lt(clave, valor));
						} else if (comparador.equals("≤ / lte")) {
							collection.deleteMany(lte(clave, valor));
						} else if (comparador.equals("!= / ne")) {
							collection.deleteMany(ne(clave, valor));
						} else if (comparador.equals("() in")) {
							collection.deleteMany(in(clave, valor));
						} else if (comparador.equals("!() nin")) {
							collection.deleteMany(nin(clave, valor));
						}

					}
				}
			}else {
				if(isNumeric(valor)) {
					
					int valorInt = Integer.parseInt(valor);
					
					if(tipoQuerry.get("Eliminar").equals("Uno")) {
						collection.deleteOne(eq(clave, valorInt));
					}else {
						collection.deleteMany(eq(clave, valorInt));
					}
				}else {
					if(tipoQuerry.get("Eliminar").equals("Uno")) {
						collection.deleteOne(eq(clave, valor));
					}else {
						collection.deleteMany(eq(clave, valor));
					}
				}
				
			}
			result = true;
		}
		return result;
	}

	/**
	 * Create the frame.
	 */
	public Frame() {

		// Base de datos por defecto
		//database = mongoClient.getDatabase("Example");
		// Colección por defecto
		//collection = database.getCollection("Students");

		informacion.put("Tipo", "AccesoDB");
		tipoQuerry.put("Actualizar", "Uno");

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		setBounds(100, 100, 1200, 859);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);

		JButton btnAccederBD = new JButton("Acceder Base Datos");
		btnAccederBD.setBackground(new Color(0, 191, 255));
		btnAccederBD.setBounds(10, 65, 185, 23);
		panel.add(btnAccederBD);

		JLabel lblBienvenido = new JLabel("Bienvenido al Controlador de MongoDB en Java\r\n");
		lblBienvenido.setBounds(363, 11, 363, 23);
		panel.add(lblBienvenido);

		JLabel lblIntroducir = new JLabel("Introduce el nombre de la base de datos:");
		lblIntroducir.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblIntroducir.setBounds(241, 60, 314, 33);
		panel.add(lblIntroducir);

		JButton btnCrearColeccion = new JButton("Crear Colecci\u00F3n");
		btnCrearColeccion.setBackground(new Color(255, 0, 0));
		btnCrearColeccion.setBounds(10, 167, 185, 23);
		panel.add(btnCrearColeccion);

		JButton btnMostrarColecciones = new JButton("Mostrar Colecciones");
		btnMostrarColecciones.setBackground(new Color(255, 0, 0));
		btnMostrarColecciones.setBounds(10, 269, 185, 23);
		panel.add(btnMostrarColecciones);

		JButton btnEliminarColeccion = new JButton("Eliminar Colección");
		btnEliminarColeccion.setBackground(new Color(255, 0, 0));
		btnEliminarColeccion.setBounds(10, 303, 185, 23);
		panel.add(btnEliminarColeccion);

		JButton btnContarNumeroDocumentos = new JButton("Contar Nº Documentos");
		btnContarNumeroDocumentos.setBackground(new Color(34, 139, 34));
		btnContarNumeroDocumentos.setBounds(10, 337, 185, 23);
		panel.add(btnContarNumeroDocumentos);

		JButton btnObtenerDocumento = new JButton("Obtener 1º Documento");
		btnObtenerDocumento.setBackground(new Color(34, 139, 34));
		btnObtenerDocumento.setBounds(10, 371, 185, 23);
		panel.add(btnObtenerDocumento);

		JButton btnQuerryConFiltros = new JButton("Querry con filtros");
		btnQuerryConFiltros.setBackground(new Color(34, 139, 34));
		btnQuerryConFiltros.setBounds(10, 439, 185, 23);
		panel.add(btnQuerryConFiltros);

		JButton btnActualizarDocumento = new JButton("Actualizar documento");
		btnActualizarDocumento.setBackground(new Color(34, 139, 34));
		btnActualizarDocumento.setBounds(10, 507, 185, 23);
		panel.add(btnActualizarDocumento);

		JButton btnEliminarDocumento = new JButton("Eliminar documento");
		btnEliminarDocumento.setBackground(new Color(34, 139, 34));
		btnEliminarDocumento.setBounds(10, 575, 185, 23);
		panel.add(btnEliminarDocumento);

		JTextPane textPane = new JTextPane();
		textPane.setForeground(Color.WHITE);
		textPane.setEditable(false);
		textPane.setBounds(241, 249, 869, 485);
		panel.add(textPane);

		tfQuerry = new JTextField();
		tfQuerry.setBounds(639, 63, 404, 37);
		panel.add(tfQuerry);
		tfQuerry.setColumns(10);

		JButton btnEnviar = new JButton("Enviar");
		btnEnviar.setBounds(1053, 65, 111, 33);
		panel.add(btnEnviar);
		btnEnviar.setIcon(null);

		JButton btnNewButton = new JButton("New button");
		btnNewButton.setBounds(278, 320, 89, 23);
		panel.add(btnNewButton);

		JButton btnInsertarDocumento = new JButton("Insertar documento");
		btnInsertarDocumento.setBackground(new Color(34, 139, 34));
		btnInsertarDocumento.setBounds(10, 473, 185, 23);
		panel.add(btnInsertarDocumento);

		JButton btnCrearBaseDatos = new JButton("Crear Base Datos");
		btnCrearBaseDatos.setBackground(new Color(0, 191, 255));
		btnCrearBaseDatos.setBounds(10, 31, 185, 23);
		panel.add(btnCrearBaseDatos);

		JButton btnAccederColeccion = new JButton("Acceder Colecci\u00F3n");
		btnAccederColeccion.setBackground(new Color(255, 0, 0));
		btnAccederColeccion.setBounds(10, 201, 185, 23);
		panel.add(btnAccederColeccion);

		JButton btnEliminarBaseDatos = new JButton("Eliminar Base Datos");
		btnEliminarBaseDatos.setBackground(new Color(0, 191, 255));
		btnEliminarBaseDatos.setBounds(10, 133, 185, 23);
		panel.add(btnEliminarBaseDatos);

		tfUpdate = new JTextField();
		tfUpdate.setColumns(10);
		tfUpdate.setBounds(639, 107, 404, 37);
		panel.add(tfUpdate);
		tfUpdate.setVisible(false);

		JLabel lblIntroducirUpdate = new JLabel("Dato Antiguo");
		lblIntroducirUpdate.setBounds(241, 109, 314, 33);
		panel.add(lblIntroducirUpdate);
		lblIntroducirUpdate.setVisible(false);

		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.setBounds(1053, 109, 111, 33);
		panel.add(btnBorrar);

		JButton btnMostrarBasesDatos = new JButton("Mostrar Bases Datos");
		btnMostrarBasesDatos.setBackground(new Color(0, 191, 255));
		btnMostrarBasesDatos.setBounds(10, 99, 185, 23);
		panel.add(btnMostrarBasesDatos);

		JLabel lblIntroducirUpdateNuevoValor = new JLabel("Dato Nuevo");
		lblIntroducirUpdateNuevoValor.setBounds(241, 162, 314, 32);
		lblIntroducirUpdateNuevoValor.setVisible(false);
		panel.add(lblIntroducirUpdateNuevoValor);

		tfUpdateNuevoValor = new JTextField();
		tfUpdateNuevoValor.setColumns(10);
		tfUpdateNuevoValor.setBounds(639, 160, 404, 37);
		tfUpdateNuevoValor.setVisible(false);
		panel.add(tfUpdateNuevoValor);

		JSeparator separator = new JSeparator();
		separator.setForeground(new Color(0, 0, 0));
		separator.setBounds(241, 235, 869, 2);
		panel.add(separator);

		JToggleButton tglbtnUpdate = new JToggleButton("Act. Solo Uno");
		tglbtnUpdate.setBounds(1053, 157, 111, 33);
		tglbtnUpdate.setVisible(false);
		panel.add(tglbtnUpdate);

		JButton btnObtenerTodosDocumentos = new JButton("Obtener todos Documentos");
		btnObtenerTodosDocumentos.setBackground(new Color(34, 139, 34));
		btnObtenerTodosDocumentos.setBounds(10, 405, 185, 23);
		panel.add(btnObtenerTodosDocumentos);

		String data[] = { "= / eq", "> / gt", "≥ / gte", "< / lt", "≤ / lte", "!= / ne", "() / in", "!() / nin" };

		JList listTipoComparador = new JList(data);
		listTipoComparador.setForeground(new Color(0, 0, 0));
		listTipoComparador.setBounds(584, 65, 53, 147);
		listTipoComparador.setVisible(false);
		listTipoComparador.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listTipoComparador.setSelectedValue("=/eq", true);

		panel.add(listTipoComparador);

		JButton btnActualizarDocumentoFiltros = new JButton("Actualizar documento (Filtros)");
		btnActualizarDocumentoFiltros.setBackground(new Color(34, 139, 34));
		btnActualizarDocumentoFiltros.setBounds(10, 541, 185, 23);
		panel.add(btnActualizarDocumentoFiltros);

		JButton btnEliminarDocumentofiltros = new JButton("Eliminar documento (Filtros)");
		btnEliminarDocumentofiltros.setBackground(new Color(34, 139, 34));
		btnEliminarDocumentofiltros.setBounds(10, 609, 185, 23);
		panel.add(btnEliminarDocumentofiltros);

		JButton btnCrearndice = new JButton("Crear Indice");
		btnCrearndice.setBackground(new Color(255, 215, 0));
		btnCrearndice.setBounds(10, 643, 185, 23);
		panel.add(btnCrearndice);

		JButton btnIndexar = new JButton("Indexar");
		btnIndexar.setBackground(new Color(255, 215, 0));
		btnIndexar.setBounds(10, 677, 185, 23);
		panel.add(btnIndexar);

		JButton btnEliminarIndice = new JButton("Eliminar Indice");
		btnEliminarIndice.setBackground(new Color(255, 215, 0));
		btnEliminarIndice.setBounds(10, 711, 185, 23);
		panel.add(btnEliminarIndice);

		String[] indexes = { "Ascendente", "Descendente", "Texto" };

		JList listIndex = new JList(indexes);
		listIndex.setBounds(565, 68, 73, 154);
		listIndex.setVisible(false);
		listTipoComparador.setSelectedValue("Ascendente", true);
		panel.add(listIndex);

		JButton btnCambiarNombreColeccion = new JButton("Cambiar nombre Colección");
		btnCambiarNombreColeccion.setBackground(Color.RED);
		btnCambiarNombreColeccion.setBounds(10, 235, 185, 23);
		panel.add(btnCambiarNombreColeccion);

		JLabel lblDB = new JLabel("DB:");
		lblDB.setBounds(801, 0, 363, 23);
		panel.add(lblDB);

		JLabel lblColeccion = new JLabel("Colección:");
		lblColeccion.setBounds(801, 29, 363, 23);
		panel.add(lblColeccion);

		String dbName = "-";
		String collectionName = "-";

		if (database == null) {
			dbName = "-";
		}else {
			dbName = database.getName();
		}
		
		if (collection == null) {
			collectionName = "-";
		}else {
			MongoNamespace name = collection.getNamespace();
			collectionName = name.getCollectionName();
		}

		lblDB.setText("DB: " + dbName);
		lblColeccion.setText("Coleccion: " + collectionName);

		// Eventos

		// Evento Crear DB
		btnCrearBaseDatos.addMouseListener(new MouseAdapter() {

			public void mouseClicked(MouseEvent e) {

				lblIntroducir.setText("Introduce el nombre de la nueva base de datos:");

				informacion.remove("Tipo");
				informacion.put("Tipo", "CrearDB");

				lblIntroducirUpdate.setBounds(241, 104, 236, 33);
				lblIntroducirUpdate.setVisible(false);
				listTipoComparador.setVisible(false);
				tfUpdate.setVisible(false);
				lblIntroducirUpdateNuevoValor.setVisible(false);
				tfUpdateNuevoValor.setVisible(false);
				tfQuerry.setVisible(true);
				tglbtnUpdate.setVisible(false);
				listIndex.setVisible(false);
				tfQuerry.setText("");
				tfUpdate.setText("");

			}
		});

		// Evento Acceso DB
		btnAccederBD.addMouseListener(new MouseAdapter() {

			public void mouseClicked(MouseEvent e) {

				lblIntroducir.setText("Introduce el nombre de la base de datos:");

				informacion.remove("Tipo");
				informacion.put("Tipo", "AccesoDB");

				lblIntroducirUpdate.setBounds(241, 104, 236, 33);
				lblIntroducirUpdate.setVisible(false);
				listTipoComparador.setVisible(false);
				tfUpdate.setVisible(false);
				lblIntroducirUpdateNuevoValor.setVisible(false);
				tfUpdateNuevoValor.setVisible(false);
				tfQuerry.setVisible(true);
				tglbtnUpdate.setVisible(false);
				listIndex.setVisible(false);
				tfQuerry.setText("");
				tfUpdate.setText("");
				textPane.setText("");
			}
		});

		// Evento Mostrar DB
		btnMostrarBasesDatos.addMouseListener(new MouseAdapter() {

			public void mouseClicked(MouseEvent e) {

				lblIntroducir.setText("Pulsa el botón de enviar para ver las distintas bases de datos:");

				informacion.remove("Tipo");
				informacion.put("Tipo", "MostrarDB");

				lblIntroducirUpdate.setBounds(241, 104, 236, 33);
				lblIntroducirUpdate.setVisible(false);
				listTipoComparador.setVisible(false);
				tfUpdate.setVisible(false);
				lblIntroducirUpdateNuevoValor.setVisible(false);
				tfUpdateNuevoValor.setVisible(false);
				tfQuerry.setVisible(false);
				tglbtnUpdate.setVisible(false);
				listIndex.setVisible(false);
				tfQuerry.setText("");
				tfUpdate.setText("");
				textPane.setText("");
			}
		});

		// Evento Eliminar DB
		btnEliminarBaseDatos.addMouseListener(new MouseAdapter() {

			public void mouseClicked(MouseEvent e) {

				lblIntroducir.setText("Introduce el nombre de la base de datos a eliminar:");

				informacion.remove("Tipo");
				informacion.put("Tipo", "EliminarDB");

				lblIntroducirUpdate.setBounds(241, 104, 236, 33);
				lblIntroducirUpdate.setVisible(false);
				listTipoComparador.setVisible(false);
				tfUpdate.setVisible(false);
				lblIntroducirUpdateNuevoValor.setVisible(false);
				tfUpdateNuevoValor.setVisible(false);
				tfQuerry.setVisible(true);
				tglbtnUpdate.setVisible(false);
				listIndex.setVisible(false);
				tfQuerry.setText("");
				tfUpdate.setText("");
				textPane.setText("");
			}
		});

		// Evento Crear Colección
		btnCrearColeccion.addMouseListener(new MouseAdapter() {

			public void mouseClicked(MouseEvent e) {

				lblIntroducir.setText("Introduce el nombre de la nueva colección:");

				informacion.remove("Tipo");
				informacion.put("Tipo", "CrearColección");

				lblIntroducirUpdate.setBounds(241, 104, 236, 33);
				lblIntroducirUpdate.setVisible(false);
				listTipoComparador.setVisible(false);
				tfUpdate.setVisible(false);
				lblIntroducirUpdateNuevoValor.setVisible(false);
				tfUpdateNuevoValor.setVisible(false);
				tfQuerry.setVisible(true);
				tglbtnUpdate.setVisible(false);
				listIndex.setVisible(false);
				tfQuerry.setText("");
				tfUpdate.setText("");
				textPane.setText("");
			}
		});

		// Evento Acceso Colección
		btnAccederColeccion.addMouseListener(new MouseAdapter() {

			public void mouseClicked(MouseEvent e) {

				lblIntroducir.setText("Introduce el nombre de la colección:");

				informacion.remove("Tipo");
				informacion.put("Tipo", "AccesoColección");

				lblIntroducirUpdate.setBounds(241, 104, 236, 33);
				lblIntroducirUpdate.setVisible(false);
				listTipoComparador.setVisible(false);
				tfUpdate.setVisible(false);
				lblIntroducirUpdateNuevoValor.setVisible(false);
				tfUpdateNuevoValor.setVisible(false);
				tfQuerry.setVisible(true);
				tglbtnUpdate.setVisible(false);
				listIndex.setVisible(false);
				tfQuerry.setText("");
				tfUpdate.setText("");
				textPane.setText("");
			}
		});

		// Evento Cambiar nombre Colección
		btnCambiarNombreColeccion.addMouseListener(new MouseAdapter() {

			public void mouseClicked(MouseEvent e) {

				lblIntroducir.setText("Introduce el nuevo nombre de la colección:");

				informacion.remove("Tipo");
				informacion.put("Tipo", "CamNombColección");

				lblIntroducirUpdate.setBounds(241, 104, 236, 33);
				lblIntroducirUpdate.setVisible(false);
				listTipoComparador.setVisible(false);
				tfUpdate.setVisible(false);
				lblIntroducirUpdateNuevoValor.setVisible(false);
				tfUpdateNuevoValor.setVisible(false);
				tfQuerry.setVisible(true);
				tglbtnUpdate.setVisible(false);
				listIndex.setVisible(false);
				tfQuerry.setText("");
				tfUpdate.setText("");
				textPane.setText("");
			}
		});

		// Evento Mostrar Colecciones
		btnMostrarColecciones.addMouseListener(new MouseAdapter() {

			public void mouseClicked(MouseEvent e) {

				lblIntroducir.setText("Pulsa el botón enviar para obtener las colecciones de la BD:");
				informacion.remove("Tipo");
				informacion.put("Tipo", "MostrarColecciones");

				lblIntroducirUpdate.setBounds(241, 104, 236, 33);
				lblIntroducirUpdate.setVisible(false);
				listTipoComparador.setVisible(false);
				tfUpdate.setVisible(false);
				lblIntroducirUpdateNuevoValor.setVisible(false);
				tfUpdateNuevoValor.setVisible(false);
				tfQuerry.setVisible(false);
				tglbtnUpdate.setVisible(false);
				listIndex.setVisible(false);
				tfQuerry.setText("");
				tfUpdate.setText("");
				textPane.setText("");
			}
		});

		// Evento Eliminar Colecciones
		btnEliminarColeccion.addMouseListener(new MouseAdapter() {

			public void mouseClicked(MouseEvent e) {

				lblIntroducir.setText("Pulsa el botón enviar para eliminar las colecciones con dicho nombre:");

				informacion.remove("Tipo");
				informacion.put("Tipo", "EliminarColecciones");

				lblIntroducirUpdate.setBounds(241, 104, 236, 33);
				lblIntroducirUpdate.setVisible(false);
				listTipoComparador.setVisible(false);
				tfUpdate.setVisible(false);
				lblIntroducirUpdateNuevoValor.setVisible(false);
				tfUpdateNuevoValor.setVisible(false);
				tfQuerry.setVisible(true);
				tglbtnUpdate.setVisible(false);
				listIndex.setVisible(false);
				tfQuerry.setText("");
				tfUpdate.setText("");
				textPane.setText("");
			}
		});

		// Evento contar documentos de una colección
		btnContarNumeroDocumentos.addMouseListener(new MouseAdapter() {

			public void mouseClicked(MouseEvent e) {

				lblIntroducir.setText("Pulsa el botón enviar para ver la cantidad de documentos de la colección:");
				informacion.remove("Tipo");
				informacion.put("Tipo", "MostrarDoc");

				lblIntroducirUpdate.setBounds(241, 104, 236, 33);
				lblIntroducirUpdate.setVisible(false);
				listTipoComparador.setVisible(false);
				tfUpdate.setVisible(false);
				lblIntroducirUpdateNuevoValor.setVisible(false);
				tfUpdateNuevoValor.setVisible(false);
				tfQuerry.setVisible(false);
				tglbtnUpdate.setVisible(false);
				listIndex.setVisible(false);
				tfQuerry.setText("");
				tfUpdate.setText("");
				textPane.setText("");
			}
		});

		// Evento obtener el primer documento de una colección
		btnObtenerDocumento.addMouseListener(new MouseAdapter() {

			public void mouseClicked(MouseEvent e) {

				lblIntroducir.setText("Pulsa el botón enviar para obtener el primer documento de la colección:");
				informacion.remove("Tipo");
				informacion.put("Tipo", "PrimerDoc");

				lblIntroducirUpdate.setBounds(241, 104, 236, 33);
				lblIntroducirUpdate.setVisible(false);
				listTipoComparador.setVisible(false);
				tfUpdate.setVisible(false);
				lblIntroducirUpdateNuevoValor.setVisible(false);
				tfUpdateNuevoValor.setVisible(false);
				tfQuerry.setVisible(false);
				tglbtnUpdate.setVisible(false);
				listIndex.setVisible(false);
				tfQuerry.setText("");
				tfUpdate.setText("");
				textPane.setText("");
			}
		});

		// Evento obtener todos los documento de una colección
		btnObtenerTodosDocumentos.addMouseListener(new MouseAdapter() {

			public void mouseClicked(MouseEvent e) {

				lblIntroducir.setText("Pulsa el botón enviar para obtener todos los documento de la colección:");
				informacion.remove("Tipo");
				informacion.put("Tipo", "TodosDoc");

				lblIntroducirUpdate.setBounds(241, 104, 236, 33);
				lblIntroducirUpdate.setVisible(false);
				listTipoComparador.setVisible(false);
				tfUpdate.setVisible(false);
				lblIntroducirUpdateNuevoValor.setVisible(false);
				tfUpdateNuevoValor.setVisible(false);
				tfQuerry.setVisible(false);
				tglbtnUpdate.setVisible(false);
				listIndex.setVisible(false);
				tfQuerry.setText("");
				tfUpdate.setText("");
				textPane.setText("");
			}
		});

		// Evento para ejecutar una querry
		btnQuerryConFiltros.addMouseListener(new MouseAdapter() {

			public void mouseClicked(MouseEvent e) {

				lblIntroducir.setText("Introduce la clave:");
				lblIntroducirUpdate.setText("Selecciona el comparador:");
				lblIntroducirUpdateNuevoValor.setText("Introduce su valor:");

				informacion.remove("Tipo");
				informacion.put("Tipo", "QuerryEquals");

				lblIntroducirUpdate.setVisible(true);
				lblIntroducirUpdate.setBounds(270, 109, 198, 33);
				listTipoComparador.setVisible(true);
				tfUpdate.setVisible(false);
				tfQuerry.setVisible(true);
				lblIntroducirUpdateNuevoValor.setVisible(true);
				tfUpdateNuevoValor.setVisible(true);
				tglbtnUpdate.setVisible(false);
				listIndex.setVisible(false);
				tfQuerry.setText("");
				tfUpdate.setText("");
				textPane.setText("");
			}
		});

		// Evento para Introducir un documento
		btnInsertarDocumento.addMouseListener(new MouseAdapter() {

			public void mouseClicked(MouseEvent e) {

				lblIntroducir.setText("Introduce los datos:");
				informacion.remove("Tipo");
				informacion.put("Tipo", "IntroducirDoc");

				lblIntroducirUpdate.setBounds(241, 104, 236, 33);
				lblIntroducirUpdate.setVisible(false);
				listTipoComparador.setVisible(false);
				tfUpdate.setVisible(false);
				tfQuerry.setVisible(true);
				lblIntroducirUpdateNuevoValor.setVisible(false);
				tfUpdateNuevoValor.setVisible(false);
				tglbtnUpdate.setVisible(false);
				listIndex.setVisible(false);
				tfQuerry.setText("");
				tfUpdate.setText("");
				textPane.setText("");
			}
		});

		// Evento para Actualizar un documento sin filtros
		btnActualizarDocumento.addMouseListener(new MouseAdapter() {

			public void mouseClicked(MouseEvent e) {

				lblIntroducir.setText("Introduce la clave:");
				lblIntroducirUpdate.setText("Introduce su valor a ser comparado:");
				lblIntroducirUpdateNuevoValor.setText("Datos/Valor Nuevos:");

				lblIntroducirUpdate.setBounds(241, 104, 236, 33);
				lblIntroducirUpdate.setVisible(true);
				listTipoComparador.setVisible(false);
				tfUpdate.setVisible(true);
				tfQuerry.setVisible(true);
				lblIntroducirUpdateNuevoValor.setVisible(true);
				tfUpdateNuevoValor.setVisible(true);
				tglbtnUpdate.setVisible(true);
				listIndex.setVisible(false);

				informacion.remove("Tipo");
				informacion.put("Tipo", "ActualizarDoc");
				tfQuerry.setText("");
				tfUpdate.setText("");
				tfUpdateNuevoValor.setText("");
				tglbtnUpdate.setText("Act. Solo Uno");
				textPane.setText("");
			}
		});

		// Evento para Actualizar un documento con filtros
		btnActualizarDocumentoFiltros.addMouseListener(new MouseAdapter() {

			public void mouseClicked(MouseEvent e) {

				lblIntroducir.setText("Clave:");
				lblIntroducirUpdate.setText("Datos/Valor Antiguos:");
				lblIntroducirUpdateNuevoValor.setText("Datos/Valor Nuevos:");

				lblIntroducirUpdate.setBounds(241, 104, 236, 33);
				lblIntroducirUpdate.setVisible(true);
				listTipoComparador.setVisible(true);
				tfUpdate.setVisible(true);
				lblIntroducirUpdateNuevoValor.setVisible(true);
				tfUpdateNuevoValor.setVisible(true);
				tglbtnUpdate.setVisible(true);
				listIndex.setVisible(false);

				informacion.remove("Tipo");
				informacion.put("Tipo", "ActualizarDocFil");
				tfQuerry.setText("");
				tfUpdate.setText("");
				tfUpdateNuevoValor.setText("");
				tglbtnUpdate.setText("Act. Solo Uno");
				textPane.setText("");
			}
		});

		// Evento para Eliminar un documento sin filtros
		btnEliminarDocumento.addMouseListener(new MouseAdapter() {

			public void mouseClicked(MouseEvent e) {

				lblIntroducir.setText("Introduce la clave del atributo a eliminar:");
				lblIntroducirUpdate.setText("Datos/Valor Antiguos:");
				informacion.remove("Tipo");
				informacion.put("Tipo", "EliminarDoc");
				tipoQuerry.remove("Eliminar");
				tipoQuerry.put("Eliminar", "Uno");

				lblIntroducirUpdate.setBounds(241, 104, 236, 33);
				lblIntroducirUpdate.setVisible(true);
				listTipoComparador.setVisible(false);
				tfUpdate.setVisible(true);
				tfQuerry.setVisible(true);
				lblIntroducirUpdateNuevoValor.setVisible(false);
				tfUpdateNuevoValor.setVisible(false);
				tglbtnUpdate.setVisible(true);
				listIndex.setVisible(false);
				tfQuerry.setText("");
				tfUpdate.setText("");
				tglbtnUpdate.setText("Elm. Solo Uno");
				textPane.setText("");
			}
		});

		// Evento para Eliminar un documento con filtros
		btnEliminarDocumentofiltros.addMouseListener(new MouseAdapter() {

			public void mouseClicked(MouseEvent e) {

				lblIntroducir.setText("Introduce la clave del atributo a eliminar:");
				lblIntroducirUpdate.setText("Datos/Valor:");
				informacion.remove("Tipo");
				informacion.put("Tipo", "EliminarDocFil");
				tipoQuerry.remove("Eliminar");
				tipoQuerry.put("Eliminar", "Uno");

				lblIntroducirUpdate.setBounds(241, 104, 236, 33);
				lblIntroducirUpdate.setVisible(true);
				listTipoComparador.setVisible(true);
				tfUpdate.setVisible(true);
				tfQuerry.setVisible(true);
				lblIntroducirUpdateNuevoValor.setVisible(false);
				tfUpdateNuevoValor.setVisible(false);
				tglbtnUpdate.setVisible(true);
				listIndex.setVisible(false);
				tfQuerry.setText("");
				tfUpdate.setText("");
				tglbtnUpdate.setText("Elm. Solo Uno");
				textPane.setText("");
			}
		});

		// Evento para crear indices
		btnCrearndice.addMouseListener(new MouseAdapter() {

			public void mouseClicked(MouseEvent e) {

				lblIntroducir.setText("Introduce el atributo a indexar");
				informacion.remove("Tipo");
				informacion.put("Tipo", "CrearIndex");

				lblIntroducirUpdate.setBounds(241, 104, 236, 33);
				lblIntroducirUpdate.setVisible(false);
				listTipoComparador.setVisible(false);
				tfUpdate.setVisible(false);
				tfQuerry.setVisible(true);
				lblIntroducirUpdateNuevoValor.setVisible(false);
				tfUpdateNuevoValor.setVisible(false);
				tglbtnUpdate.setVisible(false);
				listIndex.setVisible(true);
				tfQuerry.setText("");
				tfUpdate.setText("");
				textPane.setText("");
			}
		});

		// Evento para indexar
		btnIndexar.addMouseListener(new MouseAdapter() {

			public void mouseClicked(MouseEvent e) {

				lblIntroducir.setText("Pulsa el botón enviar para comenzar a indexar");
				informacion.remove("Tipo");
				informacion.put("Tipo", "Indexar");

				lblIntroducirUpdate.setBounds(241, 104, 236, 33);
				lblIntroducirUpdate.setVisible(false);
				listTipoComparador.setVisible(false);
				tfUpdate.setVisible(false);
				tfQuerry.setVisible(false);
				lblIntroducirUpdateNuevoValor.setVisible(false);
				tfUpdateNuevoValor.setVisible(false);
				tglbtnUpdate.setVisible(false);
				listIndex.setVisible(false);
				tfQuerry.setText("");
				tfUpdate.setText("");
				textPane.setText("");
			}
		});

		// Evento para eliminar indices
		btnEliminarIndice.addMouseListener(new MouseAdapter() {

			public void mouseClicked(MouseEvent e) {

				lblIntroducir.setText("Introduce el atributo cuyo indice quieres eliminar");
				informacion.remove("Tipo");
				informacion.put("Tipo", "EliminarIndex");

				lblIntroducirUpdate.setBounds(241, 104, 236, 33);
				lblIntroducirUpdate.setVisible(false);
				listTipoComparador.setVisible(false);
				tfUpdate.setVisible(false);
				tfQuerry.setVisible(true);
				lblIntroducirUpdateNuevoValor.setVisible(false);
				tfUpdateNuevoValor.setVisible(false);
				tglbtnUpdate.setVisible(false);
				listIndex.setVisible(true);
				tfQuerry.setText("");
				tfUpdate.setText("");
				textPane.setText("");
			}
		});

		// Evento para el botón enviar
		btnEnviar.addMouseListener(new MouseAdapter() {

			public void mouseClicked(MouseEvent e) {

				textPane.setForeground(new Color(0, 0, 0));
				textPane.setText("");
				String datoHashMap = (String) informacion.get("Tipo");

				if (datoHashMap.equals("AccesoDB")) { // Acceso a la BD

					database = mongoClient.getDatabase(tfQuerry.getText());

					boolean contiene = false;
					for (String name : listDB) {

						if (name.equals(tfQuerry.getText())) {
							contiene = true;
						}
					}

					if (contiene) {
						textPane.setForeground(new Color(0, 158, 2));
						textPane.setText("Has accedido con éxito a la BD");
						database = mongoClient.getDatabase(tfQuerry.getText());
						collection = null;

					} else {
						textPane.setForeground(new Color(255, 0, 0));
						textPane.setText("Lo sentimos pero la base de datos introducida no existe");
					}

				} else if (datoHashMap.equals("CrearDB")) { // Crear la BD

					boolean contiene = false;
					for (String name : listDB) {

						if (name.equals(tfQuerry.getText())) {
							contiene = true;
						}
					}

					if (contiene) {

						textPane.setForeground(new Color(255, 0, 0));
						textPane.setText("Lo sentimos pero la base de datos introducida ya existe");
					} else {
						textPane.setForeground(new Color(0, 158, 2));
						textPane.setText("Has creado con éxito la BD");
						database = mongoClient.getDatabase(tfQuerry.getText());
						listDB.add(tfQuerry.getText());
						collection = null;
					}
				} else if (datoHashMap.equals("MostrarDB")) { // Mostrar la BD

					StringBuffer bf = new StringBuffer();
					bf.append("El nombre de las bases de datos es: \n");

					for (String name : listDB) {
						bf.append(name).append("\n");
					}
					textPane.setText(bf.toString());

				} else if (datoHashMap.equals("EliminarDB")) { // Eliminar la BD

					database = mongoClient.getDatabase(tfQuerry.getText());

					boolean contiene = false;
					for (String name : listDB) {

						if (name.equals(tfQuerry.getText())) {
							contiene = true;
						}
					}

					if (contiene) {
						textPane.setForeground(new Color(0, 158, 2));
						textPane.setText("Has eliminado con éxito a la BD");
						database.drop();
						listDB.remove(tfQuerry.getText());
						database = null;
						collection = null;
					} else {
						textPane.setForeground(new Color(255, 0, 0));
						textPane.setText("Lo sentimos pero la base de datos introducida no existe");
					}
				} else if (datoHashMap.equals("CrearColección")) { // Crear la colección

					MongoIterable<String> it = database.listCollectionNames();

					boolean contiene = false;
					for (String name : it) {

						if (name.equals(tfQuerry.getText())) {
							contiene = true;
						}

					}

					if (contiene) {
						textPane.setForeground(new Color(255, 0, 0));
						textPane.setText("Lo sentimos pero la colección introducida ya existe");
					} else {
						textPane.setForeground(new Color(0, 158, 2));
						textPane.setText("Has creado con éxito la colección");
						collection = database.getCollection(tfQuerry.getText());
						collection.insertOne(new Document());
					}
				} else if (datoHashMap.equals("AccesoColección")) { // Acceso a la colección

					collection = database.getCollection(tfQuerry.getText());
					MongoIterable<String> it = database.listCollectionNames();

					boolean contiene = false;
					for (String name : it) {

						if (name.equals(tfQuerry.getText())) {
							contiene = true;
						}

					}

					if (contiene) {
						textPane.setForeground(new Color(0, 158, 2));
						textPane.setText("Has accedido con éxito a la colección");
						collection = database.getCollection(tfQuerry.getText());
					} else {
						textPane.setForeground(new Color(255, 0, 0));
						textPane.setText(
								"Lo sentimos pero la colección introducida no existe o no esta bien escrita (Ten en cuenta las Mayúsculas)");
					}

				} else if (datoHashMap.equals("CamNombColección")) { // Cambiar nombre de la colección

					if (collection != null) {
						MongoNamespace name = new MongoNamespace(database.getName(), tfQuerry.getText());

						collection.renameCollection(name);
						textPane.setForeground(new Color(0, 158, 2));
						textPane.setText("Has cambiado el nombre de la colección a: " + tfQuerry.getText());
					} else {
						textPane.setForeground(new Color(255, 0, 0));
						textPane.setText(
								"Lo siento, pero debes acceder a una colección antes parta realizar esta operación");
					}

				} else if (datoHashMap.equals("MostrarColecciones")) { // Mostrar la colección

					StringBuffer bf = new StringBuffer();
					bf.append("El nombre de las colecciones es: \n");

					for (String name : database.listCollectionNames()) {
						bf.append(name).append("\n");
					}
					textPane.setText(bf.toString());

				} else if (datoHashMap.equals("EliminarColecciones")) { // Eliminar la colección

					collection = database.getCollection(tfQuerry.getText());
					collection.drop();

					MongoIterable<String> it = database.listCollectionNames();

					boolean contiene = false;
					for (String name : it) {

						if (name.equals(tfQuerry.getText())) {
							contiene = true;
						}

					}

					if (contiene) {
						textPane.setForeground(new Color(0, 158, 2));
						textPane.setText("Has eliminado con éxito a la colección");
						collection = database.getCollection(tfQuerry.getText());
						collection.drop();
					} else {
						textPane.setForeground(new Color(255, 0, 0));
						textPane.setText(
								"Lo sentimos pero la colección introducida no existe o no esta bien escrita (Ten en cuenta las Mayúsculas)");
					}

				} else if (datoHashMap.equals("MostrarDoc")) { // Mostrar nº documentos

					if (collection != null) {

						textPane.setText("El número de documentos de la colección actual es de:  \n"
								+ collection.countDocuments());
					} else {
						textPane.setForeground(new Color(255, 0, 0));
						textPane.setText(
								"Lo siento, pero debes acceder a una colección antes parta realizar esta operación");
					}

				} else if (datoHashMap.equals("PrimerDoc")) { // Mostrar 1º documento

					if (collection != null) {

						Document myDoc = collection.find().first();
						textPane.setText("El primer documento de la colección actual es: \n" + myDoc.toJson());
					} else {
						textPane.setForeground(new Color(255, 0, 0));
						textPane.setText(
								"Lo siento, pero debes acceder a una colección antes parta realizar esta operación");
					}

				} else if (datoHashMap.equals("TodosDoc")) { // Mostrar todos los documentos

					if (collection != null) {

						String resultado = querryTodos(database, collection);
						textPane.setText("Los documentos de la colección actual son: \n" + resultado);
					} else {
						textPane.setForeground(new Color(255, 0, 0));
						textPane.setText(
								"Lo siento, pero debes acceder a una colección antes parta realizar esta operación");
					}

				} else if (datoHashMap.equals("QuerryEquals")) { // Ejecutar una querry

					if (collection != null) {
						String comparador = (String) listTipoComparador.getSelectedValue();

						String resultado = querry(database, collection, tfQuerry.getText(),
								tfUpdateNuevoValor.getText(), comparador);

						if (resultado.equals(
								"Lo siento, pero no se ha podido encontrar ningún resultado con la consulta solicitada")
								|| resultado.equals(
										"Lo siento, pero debes seleccionar un comparador para ejecutar la querry")) {

							textPane.setForeground(new Color(255, 0, 0));
							textPane.setText(resultado);
						} else {

							textPane.setText(
									"Se ha ejecutado con éxito la querry y el resultado obtenido es: \n" + resultado);
						}

					} else {
						textPane.setForeground(new Color(255, 0, 0));
						textPane.setText(
								"Lo siento, pero debes acceder a una colección antes parta realizar esta operación");
					}

				} else if (datoHashMap.equals("IntroducirDoc")) { // Introducir un documento

					if (collection != null) {

						BasicDBObject doc = BasicDBObject.parse(tfQuerry.getText());
						collection.insertOne(new Document(doc));
						textPane.setForeground(new Color(0, 158, 2));
						textPane.setText("Has introducido con éxito el documento");
					} else {
						textPane.setForeground(new Color(255, 0, 0));
						textPane.setText(
								"Lo siento, pero debes acceder a una colección antes parta realizar esta operación");
					}

				} else if (datoHashMap.equals("ActualizarDoc")) { // Actualizar un documento sin filtros

					if (collection != null) {
						boolean resultado = actualizarDocumento(database, collection, tfQuerry.getText(),
								tfUpdate.getText(), tfUpdateNuevoValor.getText(), "= / eq", false);

						if (resultado) {
							textPane.setForeground(new Color(0, 158, 2));
							textPane.setText("Se ha actualizado el elemento con éxito");
						} else {
							textPane.setForeground(new Color(255, 0, 0));
							textPane.setText("Lo siento, pero no se ha podido actualizar el documento indicado");
						}

					} else {
						textPane.setForeground(new Color(255, 0, 0));
						textPane.setText(
								"Lo siento, pero debes acceder a una colección antes parta realizar esta operación");
					}

				} else if (datoHashMap.equals("ActualizarDocFil")) { // Actualizar un documento con filtros

					if (collection != null) {
						String comparador = (String) listTipoComparador.getSelectedValue();

						boolean resultado = actualizarDocumento(database, collection, tfQuerry.getText(),
								tfUpdate.getText(), tfUpdateNuevoValor.getText(), comparador, true);

						if (resultado) {

							textPane.setForeground(new Color(0, 158, 2));
							textPane.setText("Se ha actualizado el elemento con éxito");
						} else {

							textPane.setForeground(new Color(255, 0, 0));
							textPane.setText("Lo siento, pero no se ha podido actualizar el documento indicado");
						}
					} else {
						textPane.setForeground(new Color(255, 0, 0));
						textPane.setText(
								"Lo siento, pero debes acceder a una colección antes parta realizar esta operación");
					}
					
				} else if (datoHashMap.equals("EliminarDoc")) { // Eliminar un documento

					if (collection != null) {
						
						boolean resultado = eliminarDocumento(database, collection, tfQuerry.getText(), tfUpdate.getText(),
								"= / eq", false);

						if (resultado) {

							textPane.setForeground(new Color(0, 158, 2));
							textPane.setText("Se ha eliminado el elemento con éxito");
						} else {

							textPane.setForeground(new Color(255, 0, 0));
							textPane.setText("Lo siento, pero no se ha podido eliminar el documento indicado");
						}
						
					} else {
						textPane.setForeground(new Color(255, 0, 0));
						textPane.setText(
								"Lo siento, pero debes acceder a una colección antes parta realizar esta operación");
					}

				} else if (datoHashMap.equals("EliminarDocFil")) { // Eliminar un documento con filtros

					if (collection != null) {
						String comparador = (String) listTipoComparador.getSelectedValue();

						boolean resultado = eliminarDocumento(database, collection, tfQuerry.getText(), tfUpdate.getText(),
								comparador, true);

						if (resultado) {

							textPane.setForeground(new Color(0, 158, 2));
							textPane.setText("Se ha eliminado el elemento con éxito");
						} else {

							textPane.setForeground(new Color(255, 0, 0));
							textPane.setText("Lo siento, pero no se ha podido eliminar el documento indicado");
						}
						
					} else {
						textPane.setForeground(new Color(255, 0, 0));
						textPane.setText(
								"Lo siento, pero debes acceder a una colección antes parta realizar esta operación");
					}

				} else if (datoHashMap.equals("CrearIndex")) { // Crea un index ascendente o descendente

					if (collection != null) {

						if (listIndex.getSelectedValue().equals("Ascendente")) {

							collection.createIndex(Indexes.ascending(tfQuerry.getText()));
						} else if (listIndex.getSelectedValue().equals("Descendente")) {

							collection.createIndex(Indexes.descending(tfQuerry.getText()));
						} else {

							collection.createIndex(Indexes.text(tfQuerry.getText()));
						}
						
					} else {
						textPane.setForeground(new Color(255, 0, 0));
						textPane.setText(
								"Lo siento, pero debes acceder a una colección antes parta realizar esta operación");
					}

				} else if (datoHashMap.equals("Indexar")) { // Indexa la coleccion


					if (collection != null) {

						StringBuffer buffer = new StringBuffer();
						buffer.append("Lo resultados de la indexación son: \n");

						for (Document index : collection.listIndexes()) {

							buffer.append(index.toJson() + "\n");
						}
						textPane.setText(buffer.toString());
						
					} else {
						textPane.setForeground(new Color(255, 0, 0));
						textPane.setText(
								"Lo siento, pero debes acceder a una colección antes parta realizar esta operación");
					}

				} else if (datoHashMap.equals("EliminarIndex")) { // Elimina los indices de la colección


					if (collection != null) {

						collection.dropIndex(tfQuerry.getText());
					} else {
						textPane.setForeground(new Color(255, 0, 0));
						textPane.setText(
								"Lo siento, pero debes acceder a una colección antes parta realizar esta operación");
					}

				} else {
					textPane.setForeground(new Color(255, 0, 0));
					textPane.setText(
							"Lo sentimos, ha habido un error con la consulta introducida, por favor, compruebe que esta bien introducida y luego vuelva a intentarlo");
				}

				

				String dbName = "-";
				String collectionName = "-";

				if (database == null) {
					dbName = "-";
				}else {
					dbName = database.getName();
				}
				
				if (collection == null) {
					collectionName = "-";
				}else {
					MongoNamespace name = collection.getNamespace();
					collectionName = name.getCollectionName();
				}

				lblDB.setText("DB: " + dbName);
				lblColeccion.setText("Coleccion: " + collectionName);
			}

		});
		
		//Botón de borrar
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				tfQuerry.setText("");
				tfUpdate.setText("");
				tfUpdateNuevoValor.setText("");

			}
		});

		//Bptón toggle para Actualizar/Eliminar uno o más elementos
		tglbtnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				boolean selected = tglbtnUpdate.isSelected();

				if (informacion.get("Tipo").equals("ActualizarDoc")
						|| informacion.get("Tipo").equals("ActualizarDocFil")) {
					if (!selected) {

						tglbtnUpdate.setText("Act. Solo Uno");
						tipoQuerry.put("Actualizar", "Uno");
					} else {
						tglbtnUpdate.setText("Act. Muchos");
						tipoQuerry.put("Actualizar", "Muchos");
					}
				} else if (informacion.get("Tipo").equals("EliminarDoc")
						|| informacion.get("Tipo").equals("EliminarDocFil")) {
					if (!selected) {

						tglbtnUpdate.setText("Elm. Solo Uno");
						tipoQuerry.put("Eliminar", "Uno");
					} else {
						tglbtnUpdate.setText("Elm. Muchos");
						tipoQuerry.put("Eliminar", "Muchos");
					}
				}
			}
		});

	}
}